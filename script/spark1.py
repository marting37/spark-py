
import pyspark.sql.functions as F
from pyspark import SparkContext
from pyspark.sql.types import StringType, BooleanType
from pyspark.sql import SparkSession
from pyspark.sql.functions import regexp_replace

spark = SparkSession.builder.appName("FootballApp").config("spark.ui.port", "4040").getOrCreate()
logger = SparkContext._jvm.org.apache.log4j
logger.LogManager.getLogger("org").setLevel(logger.Level.ERROR)
logger.LogManager.getLogger("akka").setLevel(logger.Level.ERROR)


print('Merci de bien vouloir renseigner le path du fichier .csv:')
path = input()


df = (spark.read.csv(path, header=True))
df = df.withColumnRenamed('X4', 'match')
df = df.withColumnRenamed('X6', 'competition')

df = df.drop('X2', 'X5', 'year', 'outcome', 'no')
df.printSchema()
df = df.withColumn('penalty_france', regexp_replace('penalty_france', 'NA', '0'))
df = df.withColumn('penalty_adversaire', regexp_replace('penalty_adversaire', 'NA', '0'))

df = df.filter((df.date > "1980-03-01"))


def extract_title(match):
    if match.split()[0] == "France":
        res = True
    else:
        res = False
    return res


extract_title_udf = F.udf(extract_title, BooleanType())
df = df.withColumn('domicile', extract_title_udf(df.match))


avg_but_france = df.select(F.avg("score_france")).show()
df.write.parquet("output/stats.parquet")
avg_but_adversaire = df.select(F.avg("score_adversaire"))
nbr_match = df.count()
nbr_domicile = df.filter((df.domicile == True)).count()
pc_domicile = (nbr_domicile/nbr_match)*100


def extract_coupe(name):
    if name.split()[0] == "Coupe":
        res = True
    else:
        res = False
    return res


extract_coupe_udf = F.udf(extract_coupe, BooleanType())
df_title = df.withColumn('cdm', extract_coupe_udf(df.competition))
df_title.select('competition', 'cdm')

nbr_cdm = df_title.filter((df_title.cdm == True)).count()

max_peno_france = df.select([F.max("penalty_france")])


max_peno_adv = df.select([F.max("penalty_adversaire")])

nbr_peno = (df.select([F.max("penalty_france")]).subtract(df.select([F.max("penalty_adversaire")])))




# rdd = spark.sparkContext.parallelize([('Alice', 22), ('Bob', 30)])
# df1 = spark.createDataFrame(rdd, ['name', 'age'])
# rdd2 = spark.sparkContext.parallelize([('Chris', 31), ('David', 86)])
# df2 = spark.createDataFrame(rdd2, ['name_bis', 'age_bis'])
# df1.union(df2).show()
